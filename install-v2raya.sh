#!/bin/bash
# version: v1.0.0

#https://v2raya.org/

install_from_apt() {
  # 添加公钥
  wget -qO - https://apt.v2raya.org/key/public-key.asc | sudo tee /etc/apt/trusted.gpg.d/v2raya.asc

  # 添加 V2RayA 软件源
  echo "deb https://apt.v2raya.org/ v2raya main" | sudo tee /etc/apt/sources.list.d/v2raya.list

  sudo apt update

  # 安装 V2RayA
  sudo apt install v2raya xray ## 可以使用 v2ray xray 包
}

install_from_github() {
  echo -e "请输入版本号(e.g. \033[32m2.2.5.1\033[0m): "
  version=$1
  read version

  # 检查输入是否为空
  if [ -z "$version" ]; then
      version="2.2.5.1"
  fi

  #https://github.com/v2rayA/v2rayA/releases/download/v2.2.5.1/installer_debian_x64_2.2.5.1.deb

  download_url="https://github.com/v2rayA/v2rayA/releases/download/v$version/installer_debian_x64_$version.deb"

  sudo curl -L $download_url -o /tmp/installer_debian.deb

  sudo apt install /tmp/installer_debian.deb
  
  sudo rm /tmp/installer_debian.deb

  echo -e "\033[32m 安装 v2rayA $version 成功 !!! \033[0m"
  echo -e "\033[32m sudo v2raya --reset-password 重置密码 !!! \033[0m"
}

install_from_gitlab() {
  echo -e "请输入版本号(e.g. \033[32m2.2.5.1\033[0m): "
  version=$1
  read version

  # 检查输入是否为空
  if [ -z "$version" ]; then
      version="2.2.5.1"
  fi

  # https://gitlab.com/xlogice/packages/-/raw/main/files/v2rayA/installer_debian_x64_2.2.5.1.deb

  download_url="https://gitlab.com/xlogice/packages/-/raw/main/files/v2rayA/installer_debian_x64_$version.deb"

  echo $download_url

  sudo curl -L $download_url -o /tmp/installer_debian.deb

  sudo apt install /tmp/installer_debian.deb

  sudo rm /tmp/installer_debian.deb

  echo -e "\033[32m 安装 v2rayA $version 成功 !!! \033[0m"
  echo -e "\033[32m sudo v2raya --reset-password 重置密码 !!! \033[0m"
}

#颜色
red(){
    echo -e "\033[31m\033[01m$1\033[0m"
}
green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}
yellow(){
    echo -e "\033[33m\033[01m$1\033[0m"
}
blue(){
    echo -e "\033[34m\033[01m$1\033[0m"
}

#主菜单
function start_menu(){
    clear
    red " 安装 v2rayA !!!"

    yellow " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "
    green " 1. apt 源安装 "
    green " 2. 从 github 安装"
    green " 3. 从 gitlab 安装"

    yellow " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "
    green " 0. 退出脚本"
    echo
    read -p "请输入数字:" index
    case "$index" in
        1 )
           install_from_apt
	  ;;
        2 )
           install_from_github
    ;;
        3 )
          install_from_gitlab
	  ;;
        0 )
            exit 1
        ;;
        * )
            clear
            red "请输入正确数字 !"
            start_menu
        ;;
    esac
}

start_menu "x"