#!/bin/bash
# version: v1.0.0

# 查看端口是否开放
# nc -zv <服务器IP> <端口号>
vps_server_scan_port() {
  nc -zv 81.70.167.93 9876
}

# 设置新主机名
set_new_host_name() {
  sudo hostnamectl set-hostname newname
}

# 禁止用户 shell 登录
band_user_login() {
  sudo usermod -s /usr/sbin/nologin username
}

install_some_deb() {
  apt install installer_debian.deb
}

uninstall_some_deb() {
  # 使用apt remove命令卸载软件包
  sudo apt remove xxx

  # 如果您想要删除软件包及其配置文件，您可以使用apt purge命令
  sudo apt purge xxx
}