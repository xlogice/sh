#!/bin/bash
# version: v1.0.0

test_install_d_compose() {
    echo -e "请输入版本号(e.g. \033[32m2.26.0\033[0m): "
    version=$1
    read version

    echo -e $version
    length=$(expr length "$version")
    echo -e "字符串长度为: $length"

    # 删除字符串开头和结尾的空格
    version=$(echo "$version" | awk '{$1=$1};1')

    # 检查输入是否为空
    if [ -z "$version" ]; then
        version="2.26.0"
    fi

    download_url="https://github.com/docker/compose/releases/download/v$version/docker-compose-xxxx"

    echo -e $download_url
}
