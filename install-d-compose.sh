#!/bin/bash
# version: v1.0.1

echo -e "请输入版本号(e.g. \033[32m2.26.0\033[0m): "
version=$1
read version

# 检查输入是否为空
if [ -z "$version" ]; then
    version="2.26.0"
fi

download_url="https://github.com/docker/compose/releases/download/v$version/docker-compose-$(uname -s)-$(uname -m)"

sudo curl -L $download_url -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

echo -e "\033[32m 安装 docker-compose $version 成功 !!! \033[0m"
