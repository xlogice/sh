# sh

## 免责声明

- 属于`xlogice`自用分享工具，请勿用于违法行为，否则后果自负。
- 工具中有些脚本来自互联网，不对脚本安全性负责。
- 如有侵权，联系 `xlogic.e@gamil.com`删除。


### root + 自定义密码登陆

```
bash <(curl -sSL https://gitlab.com/xlogice/sh/-/raw/main/root.sh) '[PASSWORD]'
```

### 一键删除平台监控

- 一键移除云服务器监控
- 阿里云、腾讯云、华为云、UCLOUD、甲骨文云、京东云

```
bash <(curl -sSL https://gitlab.com/xlogice/sh/-/raw/main/uninstall-dserver.sh)
```

### 多功能一键sh

```
curl -sS -O https://gitlab.com/xlogice/sh/-/raw/main/kejilion.sh && chmod +x kejilion.sh && ./kejilion.sh
```

### 安装 docker-compose

```
bash <(curl -sSL https://gitlab.com/xlogice/sh/-/raw/main/install-d-compose.sh)
```

### 安装 v2raya

```
bash <(curl -sSL https://gitlab.com/xlogice/sh/-/raw/main/install-v2raya.sh)
```